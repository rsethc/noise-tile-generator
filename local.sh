#!/bin/sh
set -eux
rm -rf built
docker build . -f builders/ubuntu/Dockerfile -t noise-tiles-ubuntu --build-arg CI_COMMIT_BRANCH="don't push" --build-arg DEBUG="-debug"
docker create --name noise-tiles-ubuntu noise-tiles-ubuntu
docker cp noise-tiles-ubuntu:/game/built built
docker rm noise-tiles-ubuntu
cd built
./noise-tiles
