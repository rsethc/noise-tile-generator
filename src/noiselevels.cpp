#include <slice.h>
#include <slicefps.h>
#include <sliceopts.h>
#define RandFloat() (rand() / (float)RAND_MAX)
#define RandFloatRange(low,high) ((low) + RandFloat() * ((high) - (low)))

slBox* base_box;
slBox* layers_box;
slBox* res_box;

void Announce (int base, int layers, int res)
{
	//printf("SizeInterval %d, %d layers, %d X %d\n",SizeInterval,n_layers,img_w,img_w);

	char* text;

	asprintf(&text,"Base: %d",base);
	base_box->SetTexRef(slRenderText(text));
	free(text);

	asprintf(&text,"Levels: %d",layers);
	layers_box->SetTexRef(slRenderText(text));
	free(text);

	asprintf(&text,"Resolution: %dx%d",res,res);
	res_box->SetTexRef(slRenderText(text));
	free(text);
};

#define MAX_RES 3200
void MkIcon (slBU* w, slBU* h, Uint8** pixels)
{
	//int SizeInterval = 4;
	int SizeInterval = 2 + rand() % 4; // 2,3,4,5
	int img_w = SizeInterval;
	int n_layers = 1;
	while (1)
	{
		int bigger = img_w * SizeInterval;
		if (bigger > MAX_RES) break;
		img_w = bigger;
		n_layers++;
	};

	Announce(SizeInterval,n_layers,img_w);
	img_w /= SizeInterval;

	int pixel_total = img_w * img_w;
	float* grid = malloc(sizeof(float) * pixel_total);
	for (int i = 0; i < pixel_total; i++) grid[i] = 0.5;

	int tile_size = img_w;
	int tiles_across = 1;
	float maxdiff = RandFloatRange(0.5,1);
	float LocalityScaling = RandFloatRange(0.7,1);
	for (int i = 0; i < n_layers; i++)
	{
		tile_size /= SizeInterval;
		tiles_across *= SizeInterval;
		maxdiff *= LocalityScaling;
		for (int y = 0; y < tiles_across; y++)
		{
			int start_y = y * tile_size;
			int stop_y = start_y + tile_size;
			for (int x = 0; x < tiles_across; x++)
			{
				float value = RandFloatRange(1-maxdiff,1+maxdiff);
				int start_x = x * tile_size;
				int stop_x = start_x + tile_size;
				for (int tile_y = start_y; tile_y < stop_y; tile_y++) for (int tile_x = start_x; tile_x < stop_x; tile_x++)
				{
					grid[tile_y * img_w + tile_x] *= value;
				};
			};
		};
	};
	float red = RandFloat();
	float green = RandFloat();
	float blue = RandFloat();
	//int less = img_w * (img_w-1);
	//Uint8* gridbytes = malloc(4 * less);
	Uint8* gridbytes = malloc(4 * pixel_total);
	//Uint8* gridbytes = malloc(3 * pixel_total);
	Uint8* byteptr = gridbytes;
	//for (int i = 0; i < less; i++)
	for (int i = 0; i < pixel_total; i++)
	{
		float value = grid[i];
		*byteptr++ = slClamp255(value * red);
		*byteptr++ = slClamp255(value * green);
		*byteptr++ = slClamp255(value * blue);
		*byteptr++ = 255;
	};
	free(grid);
	*pixels = gridbytes;
	//*w = img_w-1;
	*w = img_w;
	*h = img_w;
	//printf("[app]\tw: %d\th: %d\tdata: %lX\n",*w,*h,*pixels);
};
slBox* imgbox;
int SubID = 0;
time_t LastCapture = 0;
SDL_mutex* SaveMutex;
typedef struct
{
	char* path;
	Uint8* pixels; // RGBA32
	int w,h;
}
ImageToSave;
void SaveProc (ImageToSave* tosave)
{
	SDL_Surface* surf = SDL_CreateRGBSurfaceFrom(tosave->pixels,tosave->w,tosave->h,32,tosave->w * 4,0xFF,0xFF00,0xFF0000,0xFF000000);

	//printf("saving to \"%s\"\n",tosave->path);
	SDL_LockMutex(SaveMutex);
	//printf("doing actual save\n");
	IMG_SavePNG(surf,tosave->path); // I don't think this is thread-safe.
	SDL_UnlockMutex(SaveMutex);

	SDL_FreeSurface(surf);
	free(tosave->pixels);
	free(tosave->path);
	free(tosave);
};
void Save (char* path, Uint8* pixels, int w, int h)
{
	ImageToSave* info = malloc(sizeof(ImageToSave));
	info->path = path; info->pixels = pixels; info->w = w; info->h = h;
	//printf("starting save thread for \"%s\"\n",path);
	SDL_DetachThread(SDL_CreateThread(SaveProc,"Save PNG",info));
};
void Generate ()
{
	//Uint64 makestart = SDL_GetPerformanceCounter();

	slBU w,h; Uint8* pixels;
	MkIcon(&w,&h,&pixels);

	//Uint64 makestop = SDL_GetPerformanceCounter();

	imgbox->SetTexRef(slCreateCustomTexture(w,h,pixels));
	//SDL_Surface* surf = SDL_CreateRGBSurfaceFrom(pixels,w,h,32,w*4,0xFF,0xFF00,0xFF0000,0xFF000000);

	//Uint64 savestart = SDL_GetPerformanceCounter();

	char* path;
	time_t timenow = time(NULL);
	if (timenow == LastCapture) SubID++;
	else
	{
		LastCapture = timenow;
		SubID = 0;
	};
	asprintf(&path,"noise-icons/icon_%llu_%i.png",timenow,SubID);
	Save(path,pixels,w,h);

	//Uint64 savestop = SDL_GetPerformanceCounter();

	//printf("generation took %llu cycles, save-queueing took %llu cycles\n",makestop - makestart,savestop - savestart);
};
int main ()
{
	slInit("Noise Art Generator");
	fpsInit();
	opInit();
	SaveMutex = SDL_CreateMutex();
	system("mkdir noise-icons");
	//fpsSetIndicatorEnabled(true);
	slKeyBind* gen_bind = slGetKeyBind("Generate",slMouseButton(SDL_BUTTON_LEFT));
	gen_bind->onpress = Generate;
	//char* helptext;
	//char* keyname = slDescribeKeyInfo(gen_bind->info);
	//asprintf(&helptext,"Press %s to generate new image.",keyname);
	//free(keyname);
	imgbox = slCreateBox();//slRenderText(helptext));
	//free(helptext);
	slSetBoxDims(imgbox,0,0,1,1,100);
	base_box = slCreateBox();//slRenderText("Base: "));
	layers_box = slCreateBox();//slRenderText("Levels: "));
	res_box = slCreateBox();//slRenderText("Resolution: "));
	slSetBoxDims(base_box,0,0.88,1,0.04,80);
	slSetBoxDims(layers_box,0,0.92,1,0.04,80);
	slSetBoxDims(res_box,0,0.96,1,0.04,80);
	/*base_box->tex_wh.h = 0.9;
	layers_box->tex_wh.h = 0.9;
	res_box->tex_wh.h = 0.9;*/
	base_box->tex_align_x = slAlignLeft;
	layers_box->tex_align_x = slAlignLeft;
	res_box->tex_align_x = slAlignLeft;
	Generate();
	while (!slGetReqt()) slCycle();
	slDestroyBox(base_box);
	slDestroyBox(layers_box);
	slDestroyBox(res_box);
	SDL_DestroyMutex(SaveMutex);
	opQuit();
	fpsQuit();
	slQuit();
};
